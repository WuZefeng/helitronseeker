#!/usr/bin/env python
#coding:utf-8
# Author:   --<>
# Purpose: Matching(extracting) Helitrons from 5'Helends ans 3'Helends 
# Created: 2014/11/5

from multiprocessing import Pool
from collections import Counter
import string,time,datetime
import string,os,re
from optparse import OptionParser
import cPickle

starttime=datetime.datetime.now()
print(time.strftime("%y/%m/%d %H:%M:%S",time.localtime(time.time())))

parser=OptionParser()
parser.set_defaults(thread_number=4,min_length=200,max_length=20000,head_threshold=5,tail_threshold=5,flankh_len=50,flankt_len=50)
parser.add_option("-g","--genome", dest="genome_file", help="Input your genome file", metavar="GENOME")
parser.add_option("-f","--forward_ends", dest="forward_ends", help="Input file(.head_tail) includeing ends searching results for forward-strand", metavar="F_ENDS")
parser.add_option("-r","--reverse_ends", dest="reverse_ends", help="Input file(.head_tail) includeing ends searching results for reverse-strand", metavar="R_END")
parser.add_option("-p","--threads", dest="thread_number", help="Input the thread number(default:4)", metavar="THREADS")
parser.add_option("--maxL", dest="max_length", help="Input the max_length of helitron(default:20000)", metavar="MAXLENGTH")
parser.add_option("--minL", dest="min_length", help="Input the min_length of helitron(default:200)", metavar="MINLENGTH")
parser.add_option("--ht", dest="head_threshold", help="Input the head threshold(default:5)", metavar="HEAD_T")
parser.add_option("--tt", dest="tail_threshold", help="Input the tail threshold(default:5)", metavar="TAIL_T")
parser.add_option("--flankh", dest="flankh_len", help="Input the length of 5' flank(50bp)", metavar="FLANKH")
parser.add_option("--flankt", dest="flankt_len", help="Input the length of 3' flank(50bp)", metavar="FLANKT")
(options,args)=parser.parse_args()

min_distance=int(options.min_length)
max_distance=int(options.max_length)
threshold_h=int(options.head_threshold)
threshold_t=int(options.tail_threshold)
thread_number=int(options.thread_number)

infile=open(options.genome_file,'r')
lines=infile.readlines()
seq_len=len(lines)/2
seq_id_dict={}
seq_list=[]
seq_list_r=[]
seq_len_list=[]

def reverse_compl_seq(strseq):
    strseq=strseq.upper()
    rc_strseq=strseq.translate(string.maketrans("ATCG", "TAGC"))[::-1]
    return rc_strseq

for i in range(seq_len):
    temp_seq=lines[i*2+1].strip('\n')
    seq_id_dict[lines[i*2].split()[0][1:]]=i
    seq_list.append(temp_seq)
    seq_list_r.append(reverse_compl_seq(temp_seq))
    seq_len_list.append(len(temp_seq))
infile.close()
seq_keys=seq_id_dict.keys()
print(time.strftime("%y/%m/%d %H:%M:%S",time.localtime(time.time())))
print("Total sequence number: %d " %(seq_len))

h_file=open(options.forward_ends,'rb')
results1=cPickle.load(h_file)
h_file.close()
t_file=open(options.reverse_ends,'rb')
results2=cPickle.load(t_file)
t_file.close()
print(time.strftime("%y/%m/%d %H:%M:%S",time.localtime(time.time())))
print("Ends searching result loaded!")

def pair_ht((name,head_list,tail_list)):
    head_list=sorted(head_list)
    #print(head_list)
    tail_list=sorted(tail_list)
    #print(tail_list)
    pairs=[]
    for tpos_i in range(len(tail_list)):
        temp_h=[]
        for hpos_i in range(len(head_list)):
            if tpos_i > 0:
                if (min_distance<=(tail_list[tpos_i][0]-head_list[hpos_i][0])<=max_distance) and  head_list[hpos_i][0] >=tail_list[tpos_i-1][0]:
                    temp_h.append(head_list[hpos_i])
            else:
                if  (min_distance<=(tail_list[tpos_i][0]-head_list[hpos_i][0])<=max_distance) :
                    temp_h.append(head_list[hpos_i])
        if len(temp_h)>0:
            scores=[m[1] for m in temp_h] 
            max_score=max(scores)
            temp_list=[]
            for temp_item in temp_h:
                if temp_item[1]==max_score:
                    temp_list.append(temp_item)        
            pairs.append([temp_list,tail_list[tpos_i]])
    return([name,pairs])

filter_poss1=[]
filter_poss2=[]
for result in results1:
    filter_poss1.append([result[0],[[m, result[1][m]] for m in result[1].keys() if (result[1][m]>=threshold_h)], [[m, result[2][m]] for m in result[2].keys() if (result[2][m]>=threshold_t)]])
for result in results2:
    filter_poss2.append([result[0],[[m, result[1][m]] for m in result[1].keys() if (result[1][m]>=threshold_h)], [[m, result[2][m]] for m in result[2].keys() if (result[2][m]>=threshold_t)]])
  
pool=Pool(processes=thread_number)
res=pool.map_async(pair_ht,((m[0],m[1],m[2]) for m in filter_poss1))
pair_results1=res.get()


res=pool.map_async(pair_ht,((m[0],m[1],m[2]) for m in filter_poss2))
pair_results2=res.get()    

print("Helitrion ends-pair finding  is done!")

print("start to extracting Helitrion sequences!")
gname=os.path.basename(options.genome_file)

helitron_file_save=open(gname+"_"+str(threshold_h)+"_"+str(threshold_t)+"_helitron.fa","w")
helitron_flank_save=open(gname+"_"+str(threshold_h)+"_"+str(threshold_t)+"_helitron_with_flank.fa","w")

helitron_start_end_list=[]
temp_fasta=[]
temp_fasta1=[]
f_len_list=[]
for pair_result in pair_results1:
    if pair_result[1]<>[]:
        for i in range(len(pair_result[1])):
            info_str=""
            for j in range(1,len(pair_result[1][i][0])):
                info_str=info_str+"__"+str(pair_result[1][i][0][j][0]+1)+":"+str(pair_result[1][i][0][j][1])
            seqname=">"+pair_result[0]+'_F_' +str(pair_result[1][i][0][0][0]+1)+":"+str(pair_result[1][i][0][0][1])+"_"+str(pair_result[1][i][1][0])+":"+str(pair_result[1][i][1][1])+info_str+"\n"               
            temp_fasta.append(seqname)
            temp_fasta1.append(seqname)
            seq_start=pair_result[1][i][0][0][0]
            seq_end=pair_result[1][i][1][0]
            helitron_start_end_list.append([pair_result[0],0,[seq_start,seq_end]])
            f_len_list.append(seq_end-seq_start)
            heli_seq=seq_list[seq_id_dict[pair_result[0]]][seq_start:seq_end]
            temp_fasta.append(heli_seq+"\n")
            if seq_start-int(options.flankh_len)>=0:
                flank_h=seq_list[seq_id_dict[pair_result[0]]][(seq_start-int(options.flankh_len)):seq_start]
            else:
                flank_h="N"*abs(seq_start-int(options.flankh_len))+seq_list[seq_id_dict[pair_result[0]]][:seq_start]
            if seq_end+int(options.flankt_len)<=seq_len_list[seq_id_dict[pair_result[0]]]:
                flank_t=seq_list[seq_id_dict[pair_result[0]]][seq_end:(seq_end+int(options.flankt_len))]
            else:
                flank_t=seq_list[seq_id_dict[pair_result[0]]][seq_end:seq_len_list[seq_id_dict[pair_result[0]]]]+"N"*abs(seq_end+int(options.flankh_len))
            temp_fasta1.append(flank_h+heli_seq+flank_t+"\n")
                                                                                                                 
helitron_file_save.writelines(temp_fasta)
helitron_flank_save.writelines(temp_fasta1)
r_len_list=[]
temp_fasta=[]
temp_fasta1=[]
for pair_result in pair_results2:
    if pair_result[1]<>[]:
        for i in range(len(pair_result[1])):
            info_str=""
            for j in range(1,len(pair_result[1][i][0])):
                info_str=info_str+'__'+str(seq_len_list[seq_id_dict[pair_result[0]]]-pair_result[1][i][0][j][0])+":"+str(pair_result[1][i][0][j][1])
            seqname=">"+pair_result[0]+'_R_' +str(seq_len_list[seq_id_dict[pair_result[0]]]-pair_result[1][i][0][0][0])+":"+str(pair_result[1][i][0][0][1])+"_"+str(seq_len_list[seq_id_dict[pair_result[0]]]-pair_result[1][i][1][0]+1)+":"+str(pair_result[1][i][1][1])+info_str+"\n"               
            temp_fasta.append(seqname)
            temp_fasta1.append(seqname)
            seq_start=pair_result[1][i][0][0][0]
            seq_end=pair_result[1][i][1][0]
            helitron_start_end_list.append([pair_result[0],1,[seq_len_list[seq_id_dict[pair_result[0]]]-seq_end,seq_len_list[seq_id_dict[pair_result[0]]]-seq_start]])
            r_len_list.append(seq_end-seq_start)
            
            heli_seq=seq_list_r[seq_id_dict[pair_result[0]]][seq_start:seq_end]
            temp_fasta.append(heli_seq+"\n")
            
            if seq_start-int(options.flankh_len)>=0:
                flank_h=seq_list_r[seq_id_dict[pair_result[0]]][(seq_start-int(options.flankh_len)):seq_start]
            else:
                flank_h="N"*abs(seq_start-int(options.flankh_len))+seq_list_r[seq_id_dict[pair_result[0]]][:seq_start]
            if seq_end+int(options.flankt_len)<=seq_len_list[seq_id_dict[pair_result[0]]]:
                flank_t=seq_list_r[seq_id_dict[pair_result[0]]][seq_end:(seq_end+int(options.flankt_len))]
            else:
                flank_t=seq_list_r[seq_id_dict[pair_result[0]]][seq_end:seq_len_list[seq_id_dict[pair_result[0]]]]+"N"*abs(seq_end+int(options.flankh_len))
            temp_fasta1.append(flank_h+heli_seq+flank_t+"\n")            
            
helitron_file_save.writelines(temp_fasta)
helitron_flank_save.writelines(temp_fasta1)
helitron_file_save.close()
helitron_flank_save.close()


tempid_list=[m[0] for m in helitron_start_end_list]
seqids=set(tempid_list)
heli_map_dict={}
for seqid in seqids:
    heli_map_dict[seqid]=[[],[]]

for m in helitron_start_end_list:
    if m[1]==0:
        heli_map_dict[m[0]][0].append(m[2])
    if m[1]==1:
        heli_map_dict[m[0]][1].append(m[2])
sum_overlap=0

for dict_key in heli_map_dict.keys():
    if heli_map_dict[dict_key][0]<>[] and heli_map_dict[dict_key][1]<>[]:
        temp_seq_fill=[0 for i in range(seq_len_list[seq_id_dict[dict_key]])]
        for m1 in heli_map_dict[dict_key][0]:
            for mm in [i for i in range(m1[0],m1[1])]:
                temp_seq_fill[mm]+=1
        for m2 in heli_map_dict[dict_key][1]:
            for mm in [i for i in range(m2[0],m2[1])]:
                temp_seq_fill[mm]+=1
        sum_overlap=temp_seq_fill.count(2)



helitron_len_file=open(gname+"_"+str(threshold_h)+"_"+str(threshold_t)+"_helitron.len","w")
genome_length=sum(seq_len_list)
helitron_len_file.write("genome length: "+str(genome_length)+'\n')
helitron_len_file.write("Helitron on forward strand: "+str(len(f_len_list))+'\n')
helitron_len_file.write("total length: "+str(sum(f_len_list))+'\n')
helitron_len_file.write("Helitron on reverse strand: "+str(len(r_len_list))+'\n')
helitron_len_file.write("total length: "+str(sum(r_len_list))+'\n')
helitron_len=sum(f_len_list)+sum(r_len_list)
helitron_len_file.write("total length forward+ reverse: "+str(helitron_len)+'\n')
helitron_len_file.write("length of overlap: "+str(sum_overlap)+'\n')
helitron_len_file.write("total length forward+ reverse(no overlap): "+str(helitron_len-sum_overlap)+'\n')
helitron_len_file.write("length of each one on forward strand list below.\n")
print("genome length: "+str(genome_length)+'\n')
print("total length forward+ reverse: "+str(helitron_len))
print("length of overlap: "+str(sum_overlap))
print("total length forward+ reverse(no overlap): "+str(helitron_len-sum_overlap))


for m in f_len_list:
    helitron_len_file.write(str(m)+'\n')

helitron_len_file.write("length of each one on reverse strand list below.\n")
for m in r_len_list:
    helitron_len_file.write(str(m)+'\n')

helitron_len_file.close()

endtime=datetime.datetime.now()
print(time.strftime("%y/%m/%d %H:%M:%S",time.localtime(time.time())))
print("time used :",str(endtime-starttime))
print("well done!")
