#!/usr/bin/env python
#coding:utf-8
# Author:   --<>
# Purpose: Convert fasta file 
# Created: 2014/11/5

from  optparse import OptionParser
parser=OptionParser()
parser.add_option("-s","--seq",dest = "seq_name",help= "Input  sequence file formatted in fasta",metavar = "SEQUENCE")
parser.add_option("-o","--out",dest = "outfile_name",help = "Please set a outfile name",metavar = "OUT")
(options,args)=parser.parse_args()

infile=open(options.seq_name,'r')
outfile=open(options.outfile_name,'w')
lines=infile.readlines()

seq_info=[]
total_line=len(lines)
id_start=0
seq_start=0
seq_end=0

for i in range(total_line):
    lines[i]=lines[i].strip('\n')
    if lines[i]!='':
        if lines[i][0]==">":
            seq_end=i-1
            if seq_start<>0:
                seq_info.append([id_start,seq_start,seq_end])
            id_start=i
            seq_start=i+1
seq_end=total_line-1
seq_info.append([id_start,seq_start,seq_end])

total_len=0
for seqi in seq_info:
    current_len=sum([len(m) for m in lines[seqi[1]:(seqi[2]+1)]])
    total_len+=current_len
    outfile.write(lines[seqi[0]]+'\n')
    outfile.writelines(lines[seqi[1]:(seqi[2]+1)])
    outfile.write("\n")
outfile.close()
print("Genome size(bp):"+str(total_len)+'\n'+"contig number:"+str(len(seq_info))+'\n')
print('ok')
