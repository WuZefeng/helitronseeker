# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is a modified HelitronScanner program written in python to quickly search Helitron elements in given genomes.
* Version1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Dependence ###

* pyhton>=2.7
* Linux

### steps###

1. ** python genome_convert.py -s *genome.fa* -o genome_modified.fa** 
 
2. **python heli-scanner.py -g *genome_modified.fa* -l TrainingSet/head.lcvs -t TrainingSet/tail.lcvs -r both -p 2** 
  
3.  **python heli-pair-extract.py -g *genome_modified.fa* -f genome_modified.fa_F.head_tail -r genome_modified.fa_R.head_tail -p 2** 

### parameters ###

* Repo owner or admin
* Other community or team contact

###related paper
HelitronScanner uncovers a large overlooked cache of *Helitron* transposons in many plant genomes.