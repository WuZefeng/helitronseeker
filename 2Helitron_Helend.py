#!/usr/bin/env python
#coding:utf-8
# Author:   --<>
# Purpose: Seach Helitrons from a converted genome fasta file
# Created: 2014/11/5

from multiprocessing import Pool
from collections import Counter
import string,time,datetime
import string,os,re
from optparse import OptionParser
import cPickle

starttime=datetime.datetime.now()
print(time.strftime("%y/%m/%d %H:%M",time.localtime(time.time())))

parser=OptionParser()
parser.set_defaults(rc_strand='both',thread_number=4)
parser.add_option("-g","--genome", dest="genome_file", help="Input your genome file", metavar="GENOME")
parser.add_option("-l","--head_pattern", dest="head_pattern_file", help="Input your head pattern file", metavar="H_Pattern")
parser.add_option("-t","--tail_pattern", dest="tail_pattern_file", help="Input your tail pattern file", metavar="T_Pattern")
parser.add_option("-r","--reverse_complement", dest="rc_strand", help="Strand selection: forward,reverse,both(default:both)", metavar="STRAND")
parser.add_option("-p","--threads", dest="thread_number", help="Input the thread number(default:4)", metavar="THREADS")
(options,args)=parser.parse_args()

thread_number=int(options.thread_number)

infile=open(options.genome_file,'r')
lines=infile.readlines()
seq_len=len(lines)/2
seq_id_dict={}
seq_list=[]
seq_list_r=[]

def reverse_compl_seq(strseq):
    strseq=strseq.upper()
    rc_strseq=strseq.translate(string.maketrans("ATCG", "TAGC"))[::-1]
    return rc_strseq

for i in range(seq_len):
    temp_seq=lines[i*2+1].strip('\n')
    seq_id_dict[lines[i*2].split()[0][1:]]=i
    seq_list.append(temp_seq)
    seq_list_r.append(reverse_compl_seq(temp_seq))
infile.close()
seq_keys=seq_id_dict.keys()
print("Total sequence number: %d " %(seq_len))

h_file=open(options.head_pattern_file,'r')
h_lines=h_file.readlines()
h_patterns=[]
for line in h_lines:
    temp_str=re.compile(line.strip("\n"))
    h_patterns.append(temp_str)

t_file=open(options.tail_pattern_file,'r')
t_lines=t_file.readlines()
t_patterns=[]
for line in t_lines:
    temp_str=re.compile(line.strip("\n"))
    t_patterns.append(temp_str)
h_file.close()
t_file.close()
print("Ends pattern loaded!")



def hel_scan((name,seq)):
    h_pos=[]
    for h_pattern in h_patterns:
        h_matchs=h_pattern.finditer(seq)
        for h_match  in h_matchs:
            h_pos.append(h_match.start())
    t_pos=[]
    for t_pattern in t_patterns:
        t_matchs=t_pattern.finditer(seq)
        for t_match in t_matchs:
            t_pos.append(t_match.end())
    h_dict=Counter(h_pos)
    t_dict=Counter(t_pos)
    return([name,h_dict,t_dict])

def run_process(seq_list,strand):   
    pool=Pool(processes=thread_number)
    res=pool.map_async(hel_scan,((m,seq_list[seq_id_dict[m]]) for m in seq_keys))
    results=res.get() 
    gname=os.path.basename(options.genome_file)
    ht_file_save=open(gname+"_"+strand+".head_tail","wb")
    cPickle.dump(results, ht_file_save,True)
    ht_file_save.close()
    print("Helitrion Ends finding  is done!")
    
    
if options.rc_strand=="both":
    print("work on forward strand.")
    run_process(seq_list,"F")
    print("work on reverse complement strand.")
    run_process(seq_list_r,"R")
elif options.rc_strand=="forward":
    print("work on forward strand.")
    run_process(seq_list,"F")
else:
    print("work on reverse complement strand.")
    run_process(seq_list_r,"R")    
    
endtime=datetime.datetime.now()
print(time.strftime("%y/%m/%d %H:%M",time.localtime(time.time())))
print("time used :",str(endtime-starttime))
print("well done!")
